#!/bin/bash
set -e

cd versions
versions=( "$@" )
if [ ${#versions[@]} -eq 0 ]; then
    versions=( */ )
fi
versions=( "${versions[@]%/}" )
cd ..

for version in "${versions[@]}"; do
  echo $version;

  echo "---------------------"
  echo " Updating $version"
  echo "---------------------"
  (
    set -x
    rm -rf versions/$version/*
    cp -r README.md template/* versions/$version/
    sed -i '' 's/{{ version }}/'$version'/g' versions/$version/Dockerfile

    if [[ $version == *-cli ]] ; then
      sed -i '' '/COPY etc/d' versions/$version/Dockerfile
      sed -i '' '/RUN mkdir -p \/var/d' versions/$version/Dockerfile
      sed -i '' '/RUN a2enmod/d' versions/$version/Dockerfile
      sed -i '' '/CMD/d' versions/$version/Dockerfile
    fi
  )
  echo '>----------------------------------------------------------------<
'
done
