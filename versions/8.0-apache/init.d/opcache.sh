#!/bin/bash
set -e

# Enable/disable xdebug
if [ "$USE_OPCACHE" = "yes" ]; then
    cp -n /usr/local/etc/php/opcache.d/* /usr/local/etc/php/conf.d
else
    find /usr/local/etc/php/conf.d -name '*opcache*' -delete
fi
