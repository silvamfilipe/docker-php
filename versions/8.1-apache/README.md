# Docker image running PHP-APACHE

This image adds common things that I usually need to the official [php (apache)](https://hub.docker.com/_/php/) image. See that repo for basic usage.

## What was added?

### Extensions

* **PHP**
    * gd
    * intl
    * mysqli
    * pdo_mysql
    * zip
    * opcache
    * sockets

* **PECL**
    * xdebug (disabled by default)

#### UID and GID mapping

When mounting a volume from the host to a container, the container sees the host's owner for the files, even if it doesn't exist in the container. This image has a feature that allows setting an environment variable (`MAP_WWW_UID`) to use a directory and get www-data to have the same uid and gid as the owner of that directory. It defaults to the current working dir, or set to `no` to disable.

This is useful for example if you're using some tooling in the container to generate files inside the host volume. If this is not used, the host will have files with `uid=33 gid=33` or `uid=0 gid=0`, depending if you're using the www-data user or root.

Note that the container must be run as root, for the permission to change the www-data uid and gid.
Use `gosu` to run a command as www-data in order to use the mapped ownership.

  $ # by default the current dir is used to change www-data's uid
  $ docker run -it --rm -v $PWD:/usr/src/app -w /usr/src/app fsilva/php gosu www-data id
  uid=1000(www-data) gid=1000(www-data) groups=1000(www-data)

  $ # but you can specify another one
  $ docker run -it --rm -e MAP_WWW_UID=/data -v $PWD:/data fsilva/php gosu www-data id
  uid=1000(www-data) gid=1000(www-data) groups=1000(www-data)

  $ # or disable it by setting MAP_WWW_UID=no
  $ docker run -it --rm -e MAP_WWW_UID=no -v $PWD:/data -w /data fsilva/php gosu www-data id
  uid=33(www-data) gid=33(www-data) groups=33(www-data)

#### XDebug

  The XDebug extension is installed but disabled by default. To enable, set the enviroment variable `USE_XDEBUG=true`.
