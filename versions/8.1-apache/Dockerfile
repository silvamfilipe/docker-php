
FROM php:8.1-apache

LABEL maintainer="Filipe Silva <silvam.filipe@gmail.com>"

ENV USE_OPCACHE yes
ENV USE_XDEBUG no
ENV COMPOSER_HOME /var/www/.composer
ENV PATH vendor/bin:$COMPOSER_HOME/vendor/bin:$PATH
ENV COMPOSER_VERSION 2.7.2
ENV WEBROOT webroot

COPY bin/* /usr/local/bin/

# Install useful extensions
RUN set -eux; \
    apt-install \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libmcrypt-dev \
    libpng-dev \
    libicu-dev \
    libonig-dev \
    unzip \
    zlib1g-dev libicu-dev g++ libxml2-dev \
    git rsync zlib1g-dev \
    libzip-dev \
    gosu; \
# verify that the binary works
    gosu nobody true

RUN docker-php-ext-install mysqli pdo_mysql mbstring
RUN docker-php-ext-install gd

RUN docker-php-ext-install intl

RUN docker-php-ext-install opcache
RUN docker-php-ext-install sockets
RUN docker-php-ext-install zip

RUN mkdir -p /usr/local/etc/php/opcache.d && \
    mv /usr/local/etc/php/conf.d/*opcache.ini /usr/local/etc/php/opcache.d/

# Install APCu
RUN pecl install apcu \
    && echo "extension=apcu.so" > /usr/local/etc/php/conf.d/apcu.ini

RUN docker-php-pecl-install xdebug && \
    echo "xdebug.mode=develop,debug\nxdebug.discover_client_host=true" > /usr/local/etc/php/conf.d/xdebug.ini && \
    mkdir -p /usr/local/etc/php/xdebug.d && \
    mv /usr/local/etc/php/conf.d/*xdebug.ini /usr/local/etc/php/xdebug.d/

RUN apt-purge g++

# Include composer
RUN curl -sS https://getcomposer.org/installer | php -- \
      --install-dir=/usr/local/bin \
      --filename=composer \
      --version=${COMPOSER_VERSION}
RUN mkdir -p $COMPOSER_HOME/cache && \
    chown -R www-data:www-data /var/www && \
    echo "phar.readonly = off" > /usr/local/etc/php/conf.d/phar.ini
VOLUME $COMPOSER_HOME/cache

# Add configs
COPY etc/*.ini /usr/local/etc/php/
COPY etc/*.conf /etc/apache2/sites-available/
RUN mkdir -p /var/www/app/$WEBROOT
RUN a2enmod rewrite headers

# Add entrypoint
COPY init.d /docker-entrypoint-init.d/
COPY docker-entrypoint.sh /
ENTRYPOINT ["/docker-entrypoint.sh"]
WORKDIR /var/www/app
CMD ["apache2-foreground"]
